
1) Запускаем виртуалки:
    
		vagrant up

	Так мы поднимаем 3 виртуальные машины: 1 под ubuntu 18.04 (web1)
	и 1 под CentOS 7 (web2), еще одну под ubuntu 18.04: host, на ней и будем запускать ansible-playbook

2) Логинимся на host:

		vagrant ssh host

3) Копируем проект и заходим в папку с проектом

		git clone https://gitlab.com/gpb2/05-ansible.git && cd ansible/

4) Ставим, настраиваем и запускаем на Ubuntu nginx (192.168.1.200)
   и на CentOS nginx (192.168.1.201)

		ansible-playbook deploy_web.yml -i inventory.yml

5) Проверяем:
			
http://192.168.1.200/              
			
http://192.168.1.201/

